|Branch|Status|
|------|:--------:|
|master|[![pipeline status](https://gitlab.itsupportme.by/ansible-cm/roles/git/badges/master/pipeline.svg)](https://gitlab.itsupportme.by/ansible-cm/roles/git/commits/master)
|develop|[![pipeline status](https://gitlab.itsupportme.by/ansible-cm/roles/git/badges/develop/pipeline.svg)](https://gitlab.itsupportme.by/ansible-cm/roles/git/commits/develop)

## Ansible Role
### **_git_**

Installs Git, a distributed version control system, on RHEL/CentOS, Debian/Ubuntu and Alpine Linux system.

## Requirements

- Ansible 2.5 and higher

## Role Default Variables
```yaml
    # TODO
```

## Dependencies

None.

## License

MIT / BSD

## Author Information

This role was created in 2014 by [Jeff Geerling](https://www.jeffgeerling.com/), author of [Ansible for DevOps](https://www.ansiblefordevops.com/) and modified by ITSupportMe, LLC in 2018.
